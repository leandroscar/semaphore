## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/semaphore/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/semaphore/commits/master)


## semaphore
Bastille Template for Semaphore

## Bootstrap

```shell
ishmael ~ # bastille bootstrap https://gitlab.com/bastillebsd-templates/semaphore
```

## Usage

```shell
ishmael ~ # bastille template TARGET bastillebsd-templates/semaphore
```